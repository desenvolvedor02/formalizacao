package br.com.telugo.formalizacao.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.entidade.formalizacao.PropostaDto;
import br.com.entidade.producao.ProducaoControladoriaExibicaoDTO;
import br.com.telugo.formalizacao.config.exception.custom.BadRequestExceptionCustom;
import br.com.telugo.formalizacao.config.exception.custom.InternalServerErrorExceptionCustom;
import br.com.telugo.formalizacao.service.client.GatewayBancosClient;
import br.com.telugo.formalizacao.service.mapper.FormalizacaoMapper;
import feign.FeignException;

/**
 * Service do módulo Controle de Físicos.
 * 
 * @author desenvolvedor02
 *
 */
@Service
public class ControleDeFisicosService {

	private final GatewayBancosClient gatewayBancosClient;
	private final FormalizacaoMapper mapper;

	public ControleDeFisicosService(GatewayBancosClient gatewayBancosClient, FormalizacaoMapper mapper) {
		this.gatewayBancosClient = gatewayBancosClient;
		this.mapper = mapper;
	}

	// TODO
	public List<PropostaDto> retornarPropostasPorIntervaloData(String token, LocalDate dataInicio, LocalDate dataFim) {

		List<ProducaoControladoriaExibicaoDTO> listaProducaoControladoriaBancos;
		try {
			listaProducaoControladoriaBancos = this.gatewayBancosClient
					.retornarListaProducaoControladoriaPelaListaIdStatusTelugoEIntervaloDatas(token,
							Arrays.asList(5L, 12L), dataInicio, dataFim)
					.getBody();
		} catch (FeignException e) {
			if (e.status() == HttpStatus.NOT_FOUND.value()) {
				throw new BadRequestExceptionCustom(e.getMessage());
			}
			throw new InternalServerErrorExceptionCustom("Falha ao buscar informações no GatewayBancos.");
		}

		return this.mapper.contratoFormalizaoToPropostaDto(listaProducaoControladoriaBancos);
	}

}
