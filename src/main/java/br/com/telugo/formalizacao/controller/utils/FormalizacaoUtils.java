package br.com.telugo.formalizacao.controller.utils;

import org.springframework.stereotype.Component;

import br.com.entidade.login.UsuarioAutenticacao;
import br.com.telugo.formalizacao.config.exception.custom.BadRequestExceptionCustom;
import br.com.telugo.formalizacao.config.exception.custom.InternalServerErrorExceptionCustom;
import br.com.usuario.AutenticadorUsuario;
import br.com.usuario.exception.AutenticacaoUsuarioException.AutenticacaoIndisponivelException;
import br.com.usuario.exception.AutenticacaoUsuarioException.UsuarioNaoAutenticadoException;

/**
 * Classe com métodos utils para microsserviço de Formalização.
 * 
 * @author desenvolvedor02
 *
 */
@Component
public class FormalizacaoUtils implements AutenticadorUsuario {
	
	/** Função para verificar o usuário autenticação pelo token. */
	public UsuarioAutenticacao verificarUsuarioAutenticacao(String token) {
		// verifica se o token não está vazio
		if (token == null || token.isEmpty()) {
			throw new BadRequestExceptionCustom("Token necessário.");
		}
		try {
			return this.obterUsuario(token);
		} catch (AutenticacaoIndisponivelException e) {
			throw new BadRequestExceptionCustom("Autenticação indisponível.");
		} catch (UsuarioNaoAutenticadoException e) {
			throw new BadRequestExceptionCustom("Usuário não autenticado.");
		} catch (Exception e) {
			throw new InternalServerErrorExceptionCustom("Erro desconhecido: " + e.getMessage());
		}
	}

}
