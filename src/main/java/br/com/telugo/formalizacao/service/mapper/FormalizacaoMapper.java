package br.com.telugo.formalizacao.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import br.com.entidade.formalizacao.PropostaDto;
import br.com.entidade.producao.ProducaoControladoriaExibicaoDTO;

/**
 * Classe Mapper para os DTOs relacionados à formalizacao.
 * 
 * @author desenvolvedor02
 *
 */
@Mapper(componentModel = "spring")
public interface FormalizacaoMapper {

	/**
	 * Converte DTO {@link ProducaoControladoriaExibicaoDTO} em DTO
	 * {@link PropostaDto}.
	 */
	@Mappings({
		@Mapping(target = "uuidProposta", source = "proposta_uuid"),
		@Mapping(target = "cpfCliente", source = "cpf"),
		@Mapping(target = "valor", source = "valorProducao"),
		@Mapping(target = "dataContrato", source = "dataDigitacao"),
		@Mapping(target = "dataPagamento", source = "dataAprovacaoProposta"),
		})
	PropostaDto contratoFormalizaoToPropostaDto(ProducaoControladoriaExibicaoDTO dto);

	/**
	 * Converte uma lista de {@link ProducaoControladoriaExibicaoDTO} em uma lista
	 * de {@link PropostaDto}.
	 */
	List<PropostaDto> contratoFormalizaoToPropostaDto(List<ProducaoControladoriaExibicaoDTO> listDto);

}