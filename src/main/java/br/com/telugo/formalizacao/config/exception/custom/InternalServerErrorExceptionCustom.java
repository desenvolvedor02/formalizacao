package br.com.telugo.formalizacao.config.exception.custom;

/**
 * Classe de exceção para o erro INTERNAL_SERVER_ERROR.
 * 
 * @author desenvolvedor02
 *
 */
public class InternalServerErrorExceptionCustom extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public InternalServerErrorExceptionCustom(String msg) {
		super(msg);
	}

}
