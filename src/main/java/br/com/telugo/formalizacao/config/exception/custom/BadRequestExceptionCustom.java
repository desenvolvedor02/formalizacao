package br.com.telugo.formalizacao.config.exception.custom;

/**
 * Classe de exceção para o erro BAD_REQUEST
 * 
 * @author desenvolvedor02
 *
 */
public class BadRequestExceptionCustom extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public BadRequestExceptionCustom(String msg) {
		super(msg);
	}

}
