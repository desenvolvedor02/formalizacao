package br.com.telugo.formalizacao.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.entidade.formalizacao.PropostaDto;
import br.com.telugo.formalizacao.controller.utils.FormalizacaoUtils;
import br.com.telugo.formalizacao.service.ControleDeFisicosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller para módulo de Controle de Físicos.
 * 
 * @author desenvolvedor02
 *
 */
@RestController
@CrossOrigin("*")
@Api(value = "API para módulo de Controle de Físicos.")
@RequestMapping("/api/v1/controle-de-fisico")
public class ControleDeFisicoController {

	private final FormalizacaoUtils utils;
	private final ControleDeFisicosService service;

	public ControleDeFisicoController(FormalizacaoUtils utils, ControleDeFisicosService service) {
		this.utils = utils;
		this.service = service;
	}

	/**
	 * Endpoint para retornar propostas dos bancos por intervalo de datas. Veja:
	 * {@link ControleDeFisicosService#retornarPropostasPorIntervaloData()}
	 */
	@GetMapping("/obter-propostas")
	@ApiOperation(value = "Retorna lista de Propostas dos bancos pelo intervalo de datas.", response = PropostaDto.class, responseContainer = "List")
	public ResponseEntity<List<PropostaDto>> retornarPropostasPorIntervaloData(
			@RequestHeader("Authorization") @Valid String token,
			@RequestParam @Valid @DateTimeFormat(iso = ISO.DATE) LocalDate dataInicio,
			@RequestParam @Valid @DateTimeFormat(iso = ISO.DATE) LocalDate dataFim) {
		this.utils.verificarUsuarioAutenticacao(token);
		return ResponseEntity.ok(this.service.retornarPropostasPorIntervaloData(token, dataInicio, dataFim));
	}

}
