package br.com.telugo.formalizacao.config.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.telugo.formalizacao.config.exception.custom.BadRequestExceptionCustom;
import br.com.telugo.formalizacao.config.exception.custom.InternalServerErrorExceptionCustom;

/**
 * Classe Controller Adivice para tramtamento dos erros lançados.
 * 
 * @author desenvolvedor02
 *
 */
@RestControllerAdvice
public class ResourceExceptionHandler {
	
	/* Função para tratamento do erro BAD_REQUEST. */
	@ExceptionHandler(BadRequestExceptionCustom.class)
	public ResponseEntity<StandardError> badRequestExceptionCustom(BadRequestExceptionCustom e) {
		StandardError err = new StandardError(HttpStatus.BAD_REQUEST, e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	/* Função para tratamento do erro INTERNAL_SERVER_ERROR. */
	@ExceptionHandler(InternalServerErrorExceptionCustom.class)
	public ResponseEntity<StandardError> internalServerErrorExceptionCustom(InternalServerErrorExceptionCustom e) {
		StandardError err = new StandardError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(err);
	}

}
