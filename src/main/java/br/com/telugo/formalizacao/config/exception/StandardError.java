package br.com.telugo.formalizacao.config.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * Classe de retorno de erro padrão.
 * 
 * @author desenvolvedor02
 *
 */
@Getter
@Setter
public class StandardError {
	
	private int code;
	private String message;
	private LocalDateTime timeStamp = LocalDateTime.now();
	
	/**
	 * Construtor padrão da classe.
	 * 
	 * @param httpStatus {@link HttpStatus} do erro gerado
	 * @param mensagem a ser exibita ao usuário
	 */
	public StandardError(HttpStatus httpStatus, String mensagem) {
		this.code = httpStatus.value();
		this.message = mensagem;
	}

}
