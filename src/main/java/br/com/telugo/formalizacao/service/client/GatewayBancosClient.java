package br.com.telugo.formalizacao.service.client;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.entidade.producao.ProducaoControladoriaExibicaoDTO;

/**
 * Feign para microsserviço GatewayBancos.
 * 
 * @author desenvolvedor02
 *
 */
@FeignClient(name = "gateway-bancos", path = "/api/v1/dados-formalizacao", url = "${dev3}:3360")
public interface GatewayBancosClient {

	/**
	 * Endpoint para retonar a lista de {@link ProducaoControladoriaExibicaoDTO} de
	 * todos os bancos de acordo com lista de IDs de Status Telugo e intervalo de
	 * datas. Veja:
	 * {@link DadosFormalizacaoService#retornarListaProducaoControladoriaPelaListaIdStatusTelugoEIntervaloDatas}
	 */
	@GetMapping("/retornar-toda-producao-controladoria")
	public ResponseEntity<List<ProducaoControladoriaExibicaoDTO>> retornarListaProducaoControladoriaPelaListaIdStatusTelugoEIntervaloDatas(
			@Valid @RequestHeader(value = "Authorization") String token,
			@RequestParam @Valid List<Long> listaIdStatusTelugo,
			@RequestParam @Valid @DateTimeFormat(iso = ISO.DATE) LocalDate dataInicio,
			@RequestParam @Valid @DateTimeFormat(iso = ISO.DATE) LocalDate dataFim);

}
